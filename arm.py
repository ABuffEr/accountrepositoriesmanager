# Account Repositories Manager (ARM)
# Version: 1.0
# Author: Alberto Buffolino
import subprocess
import json
import datetime
import os

# Note: curl.exe and git.exe must be in your system path; in case, add the paths:
# %programfiles%\Git\cmd;%programfiles%\Git\bin
# or relative real paths, but in this order.

# Various parameters that you must set
# Your Bitbucket login data:
username = 'your_username'
password = 'your_password'
# URL for API requests; uncomment the correct line below
# the format is:
# apiURL = 'https://bitbucket.org/api/1.0/user'
# as it is, no parameters required, for single user account; or:
# apiURL = 'https://bitbucket.org/api/1.0/users/{team_name}'
# where {team_name} is the name of the team of which you are a member.
# ----------
# base URL for cloning repositories:
cloneURL = 'https://bitbucket.org/{user}/'
# where {user} is team or your user name.
logfile = 'arm.log'

if __name__ == '__main__':
	logLines = []
	upToDate = []
	clonedSuccess = []
	rootPath = os.getcwd()
	curl = None

	try:
		if os.path.isfile(logfile):
			os.remove(logfile)
		curl = subprocess.Popen(['curl', '--user', ''.join([username, ':', password]), apiURL], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		print 'Retrieving information from account...'
		(out, err) = curl.communicate()
		d = json.loads(out)
		for repo in d['repositories']:
			name = repo['name']
			print "%s:" %name
			(gout, gerr) = (None, None)
			if os.path.isdir(name):
				os.chdir(name)
				gitpull = subprocess.Popen(['git', 'pull'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				(gout, gerr) = gitpull.communicate()
				os.chdir(rootPath)
				if gout == 'Already up-to-date.\n':
					upToDate.append(name)
				elif gout != '' or gerr != '':
					logLines.append(''.join(['- ', name, ':\n', gout+gerr]))
			else:
				id = repo['slug']
				gitclone = subprocess.Popen(['git', 'clone', ''.join([cloneURL, id, '.git']), name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				(gout, gerr) = gitclone.communicate()
				if "Cloning" in gout and gerr == '':
					clonedSuccess.append(name)
				else:
					logLines.append(''.join(['- ', name, ':\n', gerr]))
			print "%s" %(gout+gerr)
	except WindowsError:
		err = ''
		if curl is None:
			err = 'Unable to find curl.exe, verify system PATH variable\n'
		else:
			err = 'Unable to find git.exe, verify system PATH variable\n'
		print err
		logLines.append(err)
	except Exception, e:
		print 'Exception: %s' %e
		logLines.append(str(e))
	try:
		logLines.sort()
		logLines.reverse()
		if len(clonedSuccess)>0:
			clonedSuccess.sort()
			l = ', '.join(clonedSuccess)+'\n\n'
			logLines.append(l)
			logLines.append(''.join([str(len(clonedSuccess)), ' new repos cloned successfully:\n']))
		if len(upToDate)>0:
			upToDate.sort()
			l = ', '.join(upToDate)+'\n\n'
			logLines.append(l)
			logLines.append(''.join([str(len(upToDate)), ' repos already up-to-date:\n']))
		if len(logLines)>0:
			os.chdir(rootPath)
			f = open(logfile, 'w')
			now = datetime.datetime.now()
			f.write(''.join(['Clonation log ', now.strftime('%Y-%m-%d %H:%M:%S'), '\n\n']))
			for line in reversed(logLines):
				f.write(line)
			print 'Clonation completed with errors. See log for details'
			f.close()
		else:
			print 'Clonation completed successfully. No errors'
	except Exception, e:
		print 'Errors writing log file:\n%s' %e

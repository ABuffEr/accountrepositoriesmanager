# Account Repositories Manager (ARM) #

- Author: Alberto Buffolino

ARM is a Python script to manage all repositories on your account or on a team account of which you are member.

When I say all, I mean that the script discovers the repositories names from an API answer, it's not required that it knows them before.

At the moment, it is probably only Windows compatible, and its features include only cloning and pulling via Git, on a Bitbucket account. See TODO for other future functionalities.

ARM is free software and is distributed under the [GPL2 license][1]. It's not affiliated in any way with [ARM Holdings][2] and its products. Bitbucket or other cited services are copyright of their respective owners.

## Usage ##
Simply copy the arm.py module in the folder where you want to maintain various repositories associated with a specific account, then open it and modify accordingly the parameters (username, password, etc), and run via Python shell or using the batch file provided, if you copied with .py file.

## Changes in 1.0 ##
- First implementation.
- Parameters hard-coded in module.
- Git clone and pull.
- Only Bitbucket, for now.

[1]: https://www.gnu.org/licenses/gpl-2.0.html
[2]: http://arm.com/
